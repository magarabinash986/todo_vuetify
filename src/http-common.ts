import axios from 'axios';

export const http = axios.create({
    baseURL: 'https://api-nodejs-todolist.herokuapp.com',
});
