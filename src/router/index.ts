import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import Profile from '../views/Profile.vue';
import EditUser from '../views/EditUser.vue';
import Done from '../views/Done.vue';
import Login from '../views/Index.vue';
import Register from '../views/Register.vue';
import Logout from '../components/LoggedOut.vue';
import TaskDetails from '../components/TaskDetails.vue';


Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Login',
    component: Login,
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
  },
  {
    path: '/user/edit',
    name: 'EditUser',
    component: EditUser,
  },
  {
    path: '/details/:id',
    name: 'Details',
    component: TaskDetails,
    props: true,
  },
  {
    path: '/done',
    name: 'Done',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Done,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
