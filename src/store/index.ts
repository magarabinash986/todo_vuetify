import Vue from 'vue';
import Vuex from 'vuex';
import TodoServices from '../services/TodoServices';
import LoginServices from '../services/LoginServices';
import RegisterService from '../services/RegisterServices';
import { Data } from '../types';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: Array<Data>(),
    details: null,
    routes: [
          { title: 'Home', icon: 'mdi-view-dashboard', path: '/home' },
          { title: 'Done', icon: 'mdi-help-box', path: '/done' },
          { title: 'Profile', icon: 'mdi-account-settings', path: '/profile' },
        ],
    token: localStorage.getItem('token') || null,
    user: localStorage.getItem('user') || null,
  },
  mutations: {
    setTodos: (state, data): void => {
      state.todos = data.data;
    },
    setDetails: (state, data) => {
      state.details = data.data;
    },
    addTodos: (state, data: any): void => {
      state.todos.unshift(data);
    },
    updateTodos: (state, data: any): void => {
      const index = state.todos.findIndex((t: any) => t._id === data._id);
      if (index !== -1) {
        state.todos.splice(index, 1, data);
      }
    },
    deleteTodos: (state, data): void => {
      state.todos = state.todos.filter((t: any) => t._id !== data._id);
    },
    auth: (state, data): void => {
      console.log(data);
    },
    retrieveToken: (state, token) => {
      state.token = token;
    },
    setUser: (state, user) => {
      state.user = user;
    },
    loggedout: (state) => {
      state.token = null;
      state.todos = Array<Data>();
      state.user = null;
    },
  },
  actions: {
    load: ({commit}) => {
      return new Promise((resolve, reject) => {
        TodoServices.getAll().then((res) => {
          const data = res;
          commit('setTodos', data);
          return resolve;
        })
        .catch((err) => reject(err.message));
      });
    },
    details: ({commit}, id) => {
      return new Promise((resolve, reject) => {
        TodoServices.get(id).then((res) => {
          const data = res;
          commit('setDetails', data);
          return resolve;
        }).catch((err) => {
          return reject(err.message);
        });
      });
    },
    addTodo: ({commit}, task) => {
      TodoServices.create(task).then((res) => {
        const data = res;
        commit('addTodos', data);
      })
      .catch((err) => console.log(err.message));
    },
    updateTodo: ({commit}, task) => {
      TodoServices.update(task._id, {completed: !task.completed}).then((res) => {
        const data = res;
        commit('updateTodos', data);
      })
      .catch((err) => console.log(err.message));
    },
    deleteTodo: ({commit}, task) => {
      TodoServices.delete(task._id).then((res) => {
        const data = res;
        commit('deleteTodos', task);
      })
      .catch((err) => console.log(err.message));
    },
    login: ({commit}, data) => {
      return new Promise((resolve, reject) => {
        LoginServices.login(data).then((res: any) => {
          const user = res.data.user;
          const logData = res.data.token;
          localStorage.setItem('token', (logData));
          localStorage.setItem('user', (user));
          commit('retrieveToken', logData);
          commit('setUser', user);
          resolve(res);
        })
        .catch((err: any) => {
          reject(err.message);
        });
      });
    },
    logout: ({commit}, context) => {
      return new Promise((resolve, reject) => {
        LoginServices.logout().then((res) => {
          localStorage.removeItem('token');
          localStorage.removeItem('user');
          commit('loggedout');
          return resolve;
        }).catch((err) => {
            return reject(err.message);
        });
      });
    },
    register: ({commit}, data) => {
      RegisterService.registeruser(data).then((res: any) => {
        console.log('Successfully Registered');
      });
    },
    updateUser: ({commit}, data) => {
      LoginServices.updateUser({data}).then((res: any) => {
        localStorage.setItem('user', res.data.data);
        commit('setUser', res.data.data);
      }).catch((err) => {
        console.log(err.message);
      });
    },
  },
  modules: {
  },
  getters: {
    doneTask: (state) => {
      return state.todos.filter((data: any) => data.completed === true );
    },
    loggedIn: (state) => {
      return state.token !== null;
    },
  },
});
