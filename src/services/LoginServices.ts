import axios from 'axios';
import { LoginData, Logout, User } from '@/types';
import store from '@/store';

// This can be used in one file rather then having on the multiple ones
const logged = axios.create({
    baseURL: 'https://api-nodejs-todolist.herokuapp.com',
});

class Login {
    public login(data: {email: string, password: string}): Promise<LoginData> {
        return logged.post('/user/login', data, {
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }

    public updateUser(data: any): Promise<User> {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token'),
        };
        return logged.put('/user/me', {name: data.data}, {headers});
    }

    public logout(): Promise<Logout> {
        const headers = {
            'Content-Type': 'application/json',
            'Authorization': localStorage.getItem('token'),
        };
        return logged.post('/user/logout', null, { headers});
    }
}

export default new Login();
