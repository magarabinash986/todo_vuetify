import { Todos, AddTodo, Update, Delete, Data } from '@/types';
import { http } from '@/http-common';
import store from '@/store';

class TodoServices {
    public getAll(): Promise<Todos[]> {
        return http.get('/task',  {
            headers: {
                'Content-type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            },
        })
        .then((res: any) => {
            return res.data;
        })
        .catch((err: any) => console.log(err.message));
    }

    public get(id: string): Promise<Todos> {
        return http.get(`/task/${id}`, {
            headers: {
                'Content-type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            },
        })
        .then((res) => {
            return res.data;
        }).catch((err) => {
            console.log(err.message);
        });
    }

    public create(data: any): Promise<Data> {
        return http.post('/task', data, {
            headers: {
                'Content-type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            },
        })
        .then((res: any) => {
            return res.data.data;
        }).catch((err) => {
            console.log(err.message);
        });
    }

    public update(id: string, data: any): Promise<Data> {
        return http.put(`/task/${id}`, data, {
            headers: {
                'Content-type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            },
        })
        .then((res: any) => {
            return res.data.data;
        });
    }

    public delete(id: string): Promise<Data> {
        return http.delete(`/task/${id}`, {
            headers: {
                'Content-type': 'application/json',
                'Authorization': localStorage.getItem('token'),
            },
        })
        .then((res: any) => {
            return res.data.data;
        });
    }
}

export default new TodoServices();
