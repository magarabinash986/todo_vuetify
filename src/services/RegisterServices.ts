import axios from 'axios';
import { Register } from '@/types';

const register = axios.create({
    baseURL: 'https://api-nodejs-todolist.herokuapp.com',
    headers: {
        'Content-type': 'application/json',
    },
});

class RegisterService {
    public registeruser(data: {name: string, email: string, password: string, age: number}): Promise<Register> {
        return register.post('/user/register', data);
    }
}

export default new RegisterService();
