export interface Todos {
  count: number;
  data?: (Data)[] | null;
}

// get single data

export interface AddTodo {
  success: boolean;
  data?: Data;
}

export interface Update {
  success: boolean;
  data: Data;
}

export interface Delete {
  success: boolean;
  data: {};
}


export interface Data {
  completed: boolean;
  _id: string;
  description: string;
  owner: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
}

export interface LoginData {
  user: User;
  token: string;
}
export interface User {
  age: number;
  _id: string;
  name: string;
  email: string;
  createdAt: string;
  updatedAt: string;
  __v: number;
}

export interface Register {
  user: User;
  token: string;
}

export interface Logout {
  success: boolean;
}

export interface Token {
  user: User;
  token: string;
}



// Delete User
